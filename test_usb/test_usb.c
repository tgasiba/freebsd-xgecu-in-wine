#include <stdio.h>
#include <libusb.h>

int main( void ) {
  libusb_device_handle *device_handle;
  libusb_device **devs = NULL;
  struct libusb_device_descriptor desc;

  libusb_init(NULL);

  for ( int i=0; i<libusb_get_device_list(NULL,&devs); i++ ) {
    if (LIBUSB_SUCCESS != libusb_get_device_descriptor(devs[i], &desc)) break;
    if (0x0a53==desc.idProduct && 0xa466==desc.idVendor) {
      if ( LIBUSB_SUCCESS==libusb_open(devs[i], &device_handle) &&
           LIBUSB_SUCCESS==libusb_claim_interface(device_handle, 0) ) {
        printf("OK to open device\n");
	libusb_close(device_handle);
      } else {
        printf("NOT ok to open device\n");
      }
    }
  }
  return 0;
}
